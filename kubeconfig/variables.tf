variable "cluster" {
  description = "kubernetes cluster information"
  type = object({
    name      = string
    ca_cert   = string
    host      = string
  })
}

variable "user" {
  description = "user configuration"
  type = object({
    namespace = string
    name  = string
    token = string
  })
}
