output "kubeconfig" {
  value = {
    apiVersion      = "v1"
    kind            = "Config"
    current-context = join("@", [var.user.name, var.cluster.name])

    clusters = [{
      name = var.cluster.name
      cluster = {
        certificate-authority-data = base64encode(var.cluster.ca_cert)
        server = var.cluster.host
      }
    }]

    contexts = [{
      name    = join("@", [var.user.name, var.cluster.name])
      context = {
        cluster    = var.cluster.name
        namespace = var.user.namespace
        user      = var.user.name
      }
    }]

    users = [{
      name = var.user.name
      user = {
        token = var.user.token
      }
    }]
  }
}
