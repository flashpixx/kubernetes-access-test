terraform {
  required_version = ">= 1.5"

  required_providers {
    kind = {
      source  = "tehcyx/kind"
      version = "0.2.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
  }
}

provider "kind" {}

provider "kubernetes" {
  host                   = kind_cluster.k8s.endpoint
  client_certificate     = kind_cluster.k8s.client_certificate
  client_key             = kind_cluster.k8s.client_key
  cluster_ca_certificate = kind_cluster.k8s.cluster_ca_certificate
}
