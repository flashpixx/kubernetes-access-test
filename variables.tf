variable "name" {
  description = "name of the project and all dependency elements"
  type        = string
  default     = "headlamp"
}

variable "cluster_bind_address" {
  description = "ip bind address of the cluster"
  type        = string
  default     = "127.0.0.1"
}

variable "cluster_bind_port" {
  description = "port of the cluster configuration"
  type        = number
  default     = 6500
}

variable "cluster_image" {
  description = "kind cluster image"
  type        = string
  default     = "kindest/node:v1.28.0"
}

variable "cluster_nodes" {
  description = "number of worker nodes"
  type        = number
  default     = 1
}

variable "kube_config" {
  description = "path to generated kube config file"
  type        = string
  default     = ".kubeconfig"
}

variable "role_namespace" {
  description       = "map with namespaces and roles"
  
  type              = map(object({
    namespace       = string
    roles           = list(object({
      api_groups    = list(string)
      resources     = list(string)
      verbs         = list(string)
    }))
    clusterroles    = list(object({
      api_groups    = list(string)
      resources     = list(string)
      verbs         = list(string)
    }))
  }))

  default           = {
    "test" = {
        namespace = "test"
        roles     = [
        {
            api_groups = ["*"]
            resources  = ["*"]
            verbs      = ["*"]
        }
        ]
        clusterroles = []
    }
  }
}