resource "kind_cluster" "k8s" {
  name            = var.name
  node_image      = var.cluster_image
  wait_for_ready  = true
  kubeconfig_path = var.kube_config

  kind_config {
    kind        = "Cluster"
    api_version = "kind.x-k8s.io/v1alpha4"


    networking {
      api_server_address = var.cluster_bind_address
      api_server_port = var.cluster_bind_port
    }


    node {
      role = "control-plane"
    }

    dynamic "node" {
      for_each = range(var.cluster_nodes)

      content {
        role = "worker"
      }
    }
  }
}
