# Kubernetes Restricted Access to Namespace

You need

* [Terraform](https://www.terraform.io/)
* [KinD](https://kind.sigs.k8s.io/) (with Docker)

Setup woth 

```
terraform init
terraform apply
```

it generates two `.kubeconfig` files, without suffix it is the full-admin access and with a prefix limited to a single generated namespace (equal to the prefix)
