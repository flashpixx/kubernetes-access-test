# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace
resource "kubernetes_namespace" "k8s" {
  depends_on = [ kind_cluster.k8s ]
  for_each = toset(keys(var.role_namespace))

  metadata {
    name = each.key
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role
resource "kubernetes_role_v1" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = {for k, v in var.role_namespace : k => v if length(v.roles) > 0}

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }

  dynamic "rule" {
    for_each = toset(each.value.roles)

    content {
      api_groups = rule.value.api_groups
      resources  = rule.value.resources
      verbs      = rule.value.verbs
    }
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding
resource "kubernetes_role_binding_v1" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = {for k, v in var.role_namespace : k => v if length(v.roles) > 0}

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = each.key
  }

  subject {
    kind      = "ServiceAccount"
    name      = each.key
    namespace = each.value.namespace
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account
resource "kubernetes_service_account" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = var.role_namespace

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret_v1#example-usage-service-account-token
resource "kubernetes_secret_v1" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = var.role_namespace

  type = "kubernetes.io/service-account-token"

  metadata {
    name      = each.key
    namespace = each.value.namespace
    annotations = {
      "kubernetes.io/service-account.name" = each.key
    }
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role
resource "kubernetes_cluster_role_v1" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = {for k, v in var.role_namespace : k => v if length(v.clusterroles) > 0}

  metadata {
    name      = each.key
  }

  dynamic "rule" {
    for_each = toset(each.value.clusterroles)

    content {
      api_groups = rule.value.api_groups
      resources  = rule.value.resources
      verbs      = rule.value.verbs
    }
  }
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_binding
resource "kubernetes_cluster_role_binding_v1" "k8s" {
  depends_on = [kubernetes_namespace.k8s]
  for_each   = {for k, v in var.role_namespace : k => v if length(v.clusterroles) > 0}

  metadata {
    name      = each.key
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = each.key
  }

  subject {
    kind      = "ServiceAccount"
    name      = each.key
    namespace = each.value.namespace
  }
}


# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/secret_v1
data "kubernetes_secret_v1" "k8s" {
  depends_on = [kubernetes_secret_v1.k8s]
  for_each   = var.role_namespace

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }
}

module "accounts" {
  source   = "./kubeconfig"
  for_each   = zipmap(keys(var.role_namespace), values(data.kubernetes_secret_v1.k8s))

  cluster = {
    name    = var.name
    ca_cert = kind_cluster.k8s.cluster_ca_certificate
    host    = kind_cluster.k8s.endpoint
  }

  user = {
    name      = each.key
    token     = each.value.data.token
    namespace = each.value.data.namespace
  }
}

resource "local_file" "kubeconfig" {
  for_each   = zipmap(keys(var.role_namespace), values(module.accounts))

  content  = yamlencode(each.value.kubeconfig)
  filename = "${path.module}/.kubeconfig-${each.key}"
}
